import mysql from 'mysql';

export default function set_place(req) {
    //1. Definir parametros de conexion mysql

    let conexion;

    let parametros = {
        host: 'localhost',
        user: 'root',
        password: 'colsorrento',
        database: 'wallettrip_db'
    }

    conexion = mysql.createConnection(parametros);

    //2. Conectarnos al servidor mysql
    conexion.connect(function(err) {
        if (err) {
            console.log("error" + err.message);
            return false;
        } else {
            console.log("Conectado al servidor MySQL");
            return true;
        }
    });

    //3. Realizar consulta sql

    let consulta = `SELECT idplace, name FROM place WHERE idplace=? OR idplace=?`

    conexion.query(consulta, [req.query.id, req.query.b], (err, results, fields) => {

        if (err) {
            console.error("error" + err.message)
        } else {
            console.log(results);
        }
    });

    const sitio = {
        idplace: 1,
        name: 'Universidad Distrital'
    }



    //4. Desconectarnos al servidor mysql

    conexion.end();

    


}
